#!/bin/bash
if [ ! -d "output" ]
then
    mkdir "output"
else
    rm -rf "output"
    mkdir "output"
fi
for i in $(seq 12);
    do
        echo "* Run Test$i";
        ./BuddyTest testcase/testbench/Test$i > output/Test$i.txt
        echo "--> Done Test$i";
    done
echo "--> Done all test cases."