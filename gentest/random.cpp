#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <time.h>

int random(int minN, int maxN){

    if (minN <=0) return 1;

    if (maxN < minN) return minN;

    return minN + rand() % (maxN - minN + 1);
}


int main(int argc, char* argv[]){

    if (argc < 2)
        return 0;

    srand((int) time(0));

    int page_require = atoi(argv[1]);
    int alloc_time = atoi(argv[2]);
    int value[alloc_time];
    int sum = 0;

    for(int i=0; i < alloc_time-1; i++){

        value[i] = random(1, (page_require - sum) / (alloc_time - i));

        sum += value[i];

    }

    value[alloc_time - 1] = page_require - sum;

    int swapTime = random(100, 1000);
    int tmp, A, B;

    for(int i=0; i < swapTime; i++){
        A = random(0, alloc_time/2);
        B = random(alloc_time/2, alloc_time - 1);

        tmp = value[A];
        value[A] = value[B];
        value[B] = tmp;
    }

    sum = 0;
    for (int i=0; i< alloc_time; i++){
        std::cout << value[i] << " ";
        sum+=value[i];
    }

    std::cout <<  "\r\n";// << sum;

    return 0;

}