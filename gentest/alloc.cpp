#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <time.h>


int main(int argc, char* argv[]){

    if (argc < 1)
        return 0;

    unsigned long int MAX_ORDER = atoi(argv[1]);
    bool frag_free = atoi(argv[2]) == 0 ? true : false;

    unsigned long int blockSize = 1;
    for(int i=1; i<MAX_ORDER; i++)
        blockSize *=2;

    unsigned long page = (1 << 19);

    if (frag_free){
        for(int i=0; i<= page; i+=2)
            std::cout << "0 " << i << std::endl;
    }
    else{
        for(int i=0; i<= page; i+=blockSize)
            std::cout << MAX_ORDER - 1 << " " << i << std::endl;
    }

    return 0;

}