//-----------------------------------------------------------
// Filename     : Top.cpp 
// Version	: 0.8
// Date         : 21 Jul 2019
// Description  : Global functions 
//-----------------------------------------------------------
// pRBC
//	(1) Same format as RBC
//	(2) Bank number = (RBC bank number) XOR (Least significant k bits of row number). Bank k bits.
//-----------------------------------------------------------
// Note
//	We implemented other memory maps (RCBC, pRCBC, BRC). See Top.cpp in IEEE access 2019 submitted paper.
//-----------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <iostream>

#include "Top.h"


//-------------------------
// Get row num
// 	Address map (RBC)
//-------------------------
int64_t GetRowNum_AMap_Global(int64_t nAddr) {

	return	( (uint64_t) nAddr >> (COL_WIDTH + BANK_WIDTH));  // RBC
};


//-------------------------
// Get bank num
// 	Address map (RBC)
//-------------------------
int GetBankNum_AMap_Global(int64_t nAddr) {

	uint64_t nBank_tmp = nAddr >> (COL_WIDTH);

	// return ( (unsigned) (nBank_tmp << (32-BANK_WIDTH)) >> (32-BANK_WIDTH));  // 32-bit VA
	return    ( (uint64_t) (nBank_tmp << (64-BANK_WIDTH)) >> (64-BANK_WIDTH));  // 64-bit VA
};


//-------------------------
// Get col num
// 	Address map (RBC)
//-------------------------
int GetColNum_AMap_Global(int64_t nAddr) {

	// return ( (unsigned) (nAddr << (32- COL_WIDTH)) >> (32-COL_WIDTH));  // 32-bit VA
	return    ( (uint64_t) (nAddr << (64- COL_WIDTH)) >> (64-COL_WIDTH));  // 64-bit VA
};


//-------------------------
// Get row num 
// 	Memory map
//-------------------------
// int64_t GetRowNum_MMap_Global(int64_t nAddr) {
//
//	return	( (uint64_t) nAddr >> (COL_WIDTH + BANK_WIDTH));  // RBC or RCBC or pRBC
//};


//-------------------------
// Get row num
//      Memory map
//-------------------------
int64_t GetRowNum_MMap_Global(int64_t nAddr) {

	#ifdef BRC
	uint64_t nRow_tmp = nAddr >> (COL_WIDTH);
	// uint64_t nRow = ( (uint64_t) (nRow_tmp << (32 - ROW_WIDTH)) >> (32 - ROW_WIDTH));	// Arch32. IA.
	uint64_t    nRow = ( (uint64_t) (nRow_tmp << (64 - ROW_WIDTH)) >> (64 - ROW_WIDTH));	// Arch64. IA.
	assert (nRow >= 0);
	return (nRow);
	#endif
	
	return  ( (uint64_t) nAddr >> (COL_WIDTH + BANK_WIDTH));				// RBC, RCBC, pRBC, pRCBC. IA.
};


//-------------------------
// Get bank num
//      Memory map
//-------------------------
int GetBankNum_MMap_Global(int64_t nAddr) {								// nAddr just number
 
	// uint64_t nBank_tmp = -1;									// IA.
	// uint64_t nBank = -1;
	
	// Debug
	assert (nAddr >= 0);
	
	#ifdef RBC
	uint64_t nBank_tmp = nAddr >> (COL_WIDTH);
	// uint64_t nBank = ( (uint64_t) (nBank_tmp << (32 - BANK_WIDTH)) >> (32 - BANK_WIDTH));	// RBC. Arch32
	uint64_t    nBank = ( (uint64_t) (nBank_tmp << (64 - BANK_WIDTH)) >> (64 - BANK_WIDTH));	// RBC. Arch64
	#endif
	
	#ifdef BRC
	uint64_t nBank = ( (uint64_t) nAddr >> (COL_WIDTH + ROW_WIDTH));                        	// IA.
	#endif
	
	#ifdef RCBC
	uint64_t nBank_tmp = nAddr >> (COL2_WIDTH);
	// uint64_t nBank = ( (uint64_t) (nBank_tmp << (32 - BANK_WIDTH)) >> (32 - BANK_WIDTH));	// RCBC. Arch32
	uint64_t    nBank = ( (uint64_t) (nBank_tmp << (64 - BANK_WIDTH)) >> (64 - BANK_WIDTH));	// RCBC. Arch64
	#endif
	
	#ifdef pRBC
	//------------------------------
	// (1) Get RBC bank number
	// (2) Get RBC row number
	// (3) Get least significant k bits of RBC row number
	// (4) Bank number = (RBC bank number) XOR (Least significant k bits of RBC row number)
	//------------------------------
	
	// (1) Get RBC bank number 
	uint64_t nBank_tmp = nAddr >> (COL_WIDTH);
	// uint64_t nBank = ( (uint64_t) (nBank_tmp << (32 - BANK_WIDTH)) >> (32 - BANK_WIDTH));	// RBC. Arch32
	uint64_t    nBank = ( (uint64_t) (nBank_tmp << (64 - BANK_WIDTH)) >> (64 - BANK_WIDTH));	// RBC. Arch64
	
	// // Row LSB   
	// uint64_t nRow = GetRowNum_MMap_Global(nAddr);
	// uint64_t nRowLSB = nRow & 1;  // Bitwise and         
	// 
	// // Bank LSB
	// uint64_t nBankLSB = nBank & 1;  // Bitwise and 
	// 
	// // (Row LSB) xor (Bank LSB)
	// nBankLSB = nBankLSB ^ nRowLSB;  // Bitwise xor
	// 
	// // New bank num
	// nBank = (uint64_t) ((nBank >> 1) << 1) + nBankLSB;
	
	// (2) Get RBC row number 
	uint64_t nRow = GetRowNum_MMap_Global(nAddr);
	
	// (3) Get least significant k bits of RBC row number
	uint64_t nRowLS = -1;
	if (BANK_WIDTH == 3) {		// 8 banks
	        nRowLS = nRow & 7;	// Bitwise and
	}
	else if (BANK_WIDTH == 2) {	// 4 banks
	        nRowLS = nRow & 3;
	}
	else if (BANK_WIDTH == 1) {	// 2 banks
	        nRowLS = nRow & 1;
	}
	else {
	        assert (0);
	};
	
	// (4) Get new bank number 
	nBank = nBank ^ nRowLS;		// Bitwise xor
	#endif
	
	
	#ifdef pRCBC			// Same pRBC (except RBC -> RCBC). IA.
	//------------------------------
	// (1) Get RCBC bank number
	// (2) Get RCBC row number
	// (3) Get least significant k bits of RCBC row number
	// (4) Bank number = (RCBC bank number) XOR (Least significant k bits of RCBC row number)
	//------------------------------
	
	// (1) Get RCBC bank number 
	uint64_t nBank_tmp = nAddr >> (COL2_WIDTH);
	// uint64_t nBank = ( (uint64_t) (nBank_tmp << (32 - BANK_WIDTH)) >> (32 - BANK_WIDTH));	// RCBC. Arch32
	uint64_t    nBank = ( (uint64_t) (nBank_tmp << (64 - BANK_WIDTH)) >> (64 - BANK_WIDTH));	// RCBC. Arch64
	
	// (2) Get RCBC row number 
	uint64_t nRow = GetRowNum_MMap_Global(nAddr);
	
	// (3) Get least significant k bits of RCBC row number
	uint64_t nRowLS = -1;
	if (BANK_WIDTH == 3) {		// 8 banks
	        nRowLS = nRow & 7;	// Bitwise and
	}
	else if (BANK_WIDTH == 2) {	// 4 banks
	        nRowLS = nRow & 3;
	}
	else if (BANK_WIDTH == 1) {	// 2 banks
	        nRowLS = nRow & 1;
	}
	else {
	        assert (0);
	};
	
	// (4) Get new bank number 
	nBank = nBank ^ nRowLS;		// Bitwise xor
	#endif
	
	
	// Debug
	assert (nBank >= 0);
	return nBank;
};


//-------------------------
// Get col num
//      Memor map
//-------------------------
int GetColNum_MMap_Global(int64_t nAddr) {
	
	#ifdef RBC
	// return  ( (uint64_t) (nAddr << (32 - COL_WIDTH)) >> (32 - COL_WIDTH));		// RBC. Arch32
	return     ( (uint64_t) (nAddr << (64 - COL_WIDTH)) >> (64 - COL_WIDTH));		// RBC. Arch64
	#endif
	
	#ifdef BRC
	// return ( (uint64_t) (nAddr << (32 - COL_WIDTH)) >> (32 - COL_WIDTH));		// Same RBC
	return    ( (uint64_t) (nAddr << (64 - COL_WIDTH)) >> (64 - COL_WIDTH));
	#endif
	
	#ifdef pRBC
	// return ( (uint64_t) (nAddr << (32 - COL_WIDTH)) >> (32 - COL_WIDTH));		// Same RBC
	return    ( (uint64_t) (nAddr << (64 - COL_WIDTH)) >> (64 - COL_WIDTH));
	#endif
	
	#ifdef RCBC
	// uint64_t nCol2  = (uint64_t) (nAddr << (32 - COL2_WIDTH)) >> (32 - COL2_WIDTH);	// Arch32
	uint64_t    nCol2  = (uint64_t) (nAddr << (64 - COL2_WIDTH)) >> (64 - COL2_WIDTH);	// Arch64
	
	uint64_t nCol1_tmp = nAddr >> (BANK_WIDTH + COL2_WIDTH);
	// uint64_t nCol1 = ( (uint64_t) (nCol1_tmp << (32 - BANK_WIDTH - COL2_WIDTH)) >> (32 - BANK_WIDTH - COL2_WIDTH));	// Arch32
	// uint64_t nCol1 = ( (uint64_t) (nCol1_tmp << (64 - BANK_WIDTH - COL2_WIDTH)) >> (64 - BANK_WIDTH - COL2_WIDTH));	// Arch64
	uint64_t    nCol1 = ( (uint64_t) (nCol1_tmp << (64 - COL1_WIDTH))              >> (64 - COL1_WIDTH));			// Arch64
	
	return    ( (uint64_t) (nCol1 << COL2_WIDTH) + nCol2 );					// RCBC
	#endif
	
	#ifdef pRCBC										// Same RCBC. IA.
	// uint64_t nCol2  = (uint64_t) (nAddr << (32 - COL2_WIDTH)) >> (32 - COL2_WIDTH);	// Arch32
	uint64_t    nCol2  = (uint64_t) (nAddr << (64 - COL2_WIDTH)) >> (64 - COL2_WIDTH);	// Arch64
	
	uint64_t nCol1_tmp = nAddr >> (BANK_WIDTH + COL2_WIDTH);
	// uint64_t nCol1 = ( (uint64_t) (nCol1_tmp << (32 - BANK_WIDTH - COL2_WIDTH)) >> (32 - BANK_WIDTH - COL2_WIDTH));	// Arch32
	// uint64_t nCol1 = ( (uint64_t) (nCol1_tmp << (64 - BANK_WIDTH - COL2_WIDTH)) >> (64 - BANK_WIDTH - COL2_WIDTH));	// Arch64
	uint64_t    nCol1 = ( (uint64_t) (nCol1_tmp << (64 - COL1_WIDTH))              >> (64 - COL1_WIDTH));			// Arch64
	
	return    ( (uint64_t) (nCol1 << COL2_WIDTH) + nCol2 );					// RCBC
	#endif
};




//-------------------------
// Get RBC addr (from R, B, C) 
// 	Address map (RBC only)
//-------------------------
int64_t GetAddr_AMap_Global(int64_t nRow, int nBank, int nCol) {

	return (uint64_t) ( (nRow << (BANK_WIDTH + COL_WIDTH)) + (nBank << COL_WIDTH) + (nCol) );
};


string Convert_eResult2string(EResultType eType) {

        switch(eType) {
                case ERESULT_TYPE_YES:  
			return("ERESULT_TYPE_YES");  
			break;
                case ERESULT_TYPE_NO:  
			return("ERESULT_TYPE_NO");  
			break;
                case ERESULT_TYPE_ACCEPT:  
			return("ERESULT_TYPE_ACCEPT");  
			break;
                case ERESULT_TYPE_REJECT:  
			return("ERESULT_TYPE_REJECT");  
			break;
                case ERESULT_TYPE_SUCCESS:  
			return("ERESULT_TYPE_SUCCESS");  
			break;
                case ERESULT_TYPE_FAIL:  
			return("ERESULT_TYPE_FAIL");  
			break;
                case ERESULT_TYPE_ON:  
			return("ERESULT_TYPE_ON");  
			break;
                case ERESULT_TYPE_OFF:  
			return("ERESULT_TYPE_OFF");  
			break;
                case ERESULT_TYPE_UNDEFINED:  
			return("ERESULT_TYPE_UNDEFINED");  
			break;
                default: 
			break;
        };
	return (NULL);
};


string Convert_eDir2string(ETransDirType eType) {

        switch(eType) {
                case ETRANS_DIR_TYPE_READ:  
			return("ETRANS_DIR_TYPE_READ");  
			break;
                case ETRANS_DIR_TYPE_WRITE: 
			return("ETRANS_DIR_TYPE_WRITE"); 
			break;
                case ETRANS_DIR_TYPE_UNDEFINED:    
			return("EUD_TYPE_UNDEFINED");
			break;
                default: 
			break;
        };
	return (NULL);
};


string Convert_eUDType2string(EUDType eType) {

        switch(eType) {
                case EUD_TYPE_AR:  
			return("EUD_TYPE_AR");  
			break;
                case EUD_TYPE_AW:  
			return("EUD_TYPE_AW");  
			break;
                case EUD_TYPE_R:   
			return("EUD_TYPE_R");   
			break;
                case EUD_TYPE_W:   
			return("EUD_TYPE_W");   
			break;
                case EUD_TYPE_B:   
			return("EUD_TYPE_B");   
			break;
                case EUD_TYPE_UNDEFINED:    
			return("EUD_TYPE_UNDEFINED");    
                default: 
			break;
        };
	return (NULL);
};

 
string Convert_eTransType2string(ETransType eType) {

        switch(eType) {
                case ETRANS_TYPE_NORMAL:  
			return("ETRANS_TYPE_NORMAL");  
			break;

                case ETRANS_TYPE_FIRST_PTW:  
			return("ETRANS_TYPE_FIRST_PTW");  
			break;
                case ETRANS_TYPE_SECOND_PTW:  
			return("ETRANS_TYPE_SECOND_PTW");  
			break;

                case ETRANS_TYPE_EVICT:  
			return("ETRANS_TYPE_EVICT");  
			break;
                case ETRANS_TYPE_LINE_FILL:  
			return("ETRANS_TYPE_LINE_FILL");  
			break;

                case ETRANS_TYPE_FIRST_APTW:  
			return("ETRANS_TYPE_FIRST_APTW");  
			break;
                case ETRANS_TYPE_SECOND_APTW:  
			return("ETRANS_TYPE_SECOND_APTW");  
			break;

                case ETRANS_TYPE_FIRST_RPTW:  
			return("ETRANS_TYPE_FIRST_RPTW");  
			break;
                case ETRANS_TYPE_SECOND_RPTW:  
			return("ETRANS_TYPE_SECOND_RPTW");  
			break;
                case ETRANS_TYPE_THIRD_RPTW:  
			return("ETRANS_TYPE_THIRD_RPTW");  
			break;

                case ETRANS_TYPE_UNDEFINED:  
			return("ETRANS_TYPE_UNDEFINED");  
			break;
                default: 
			break;
        };
	return (NULL);
};

