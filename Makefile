.PHONY: run

all: 
	g++ -std=c++11 -g -Wall -D"SCENARIO_7" -D"FREE_LIST = 5" -D"BCT_DISABLE" -D"MMU_ON" -D"IMG_HORIZONTAL_SIZE = 1280" -D"IMG_VERTICAL_SIZE = 720" -D"CONTIGUITY_DISABLE" -D"BUDDY_ENABLE" -o BuddyTest ./Top.cpp ./ListEntry.cpp ./CBuddy.cpp ./BlockMappingTable.cpp ./main.cpp

run:
	./BuddyTest > output.txt

clean: 
	rm -f run
	rm -f *.o
	rm -f *.out
	rm -f *.txt
	rm -f *.exe
	rm -f random
	rm -rf output/ build/