## Buddy Allocator
Implementing Simple Buddy Allocator in C/C++

## Implementation
1. Free-list
We implement free-list bases on the single linked-list structure.
Free-list includes a lot of list entry, each of it refers to the sequence of block which has the same level.

![PNG](img/freelist.png)

A block of free-list has some attributes:
- (startPPN) - start physical page number of block.
- (next) - pointer to the next block at the same level.

Free-list is a sequence of the list entry, each list entry has some attributes:
- (_length) - how many free blocks available in this level.
- (_blockSize) - each block include how many pages.
- (head) - a pointer to the list of the block.

We also have some method to do the work with free-list entry:
- set/getBlockSize() : Change block size configuration of a free-list.
- add/remove() : add/remove a block to/from free-list.
- getListLength() : return the quantity of free blocks.
- getListHead() : return the first free block.
- isEmpty() : check if no free block inside.
- find_buddy() : which make easy to find the buddy of a block (for merging in the de-allocation step).


Following the requirement, we can prototype some structure like below:
```c++
struct List_Entry_Node{

    unsigned int startPPN;
    List_Entry_Node *next;

};


class List_Entry{
private:
    List_Entry_Node *_head;
    unsigned int _length;
    unsigned int _blockSize;

public:

    List_Entry();
    ~List_Entry();
    
    bool add(unsigned int startPPN);
    bool remove(unsigned int startPPN);


    void setBlockSize(unsigned int blockSize);
    unsigned int getBlockSize();

    unsigned int  getListLength();
    bool isEmpty();
    List_Entry_Node *getListHead();

    List_Entry_Node * find_buddy(unsigned int startPPN);

};

```