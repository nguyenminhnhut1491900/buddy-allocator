#include "CBuddy.h"
#include <iostream>
#include <fstream>
#include <chrono>

bool fexists(const std::string& filename){
    std::ifstream ifile(filename);
    return (bool) ifile;
}
#ifdef SCENARIO_1
    string scenario="SCENARIO_1";
    int loop=2;
#endif
#ifdef SCENARIO_2
    string scenario="SCENARIO_2";
    int loop=2;
#endif
#ifdef SCENARIO_5
    string scenario="SCENARIO_5";
    int loop=4;
#endif
#ifdef SCENARIO_6
    string scenario="SCENARIO_6";
    int loop=3;
#endif
#ifdef SCENARIO_7
    string scenario="SCENARIO_7";
    int loop=1;
#endif
int main(int argc, char* argv[]){
    
    //--------------------------------Initial FreeList---------------------------------------
    // std::cout << "(*) Initial FreeList: --------------------------------" << std::endl;
    auto start = std::chrono::high_resolution_clock::now();

    CBuddy * cpBuddy = new CBuddy("Buddy_Allocator");

    cpBuddy->Set_FreeList(9);

    auto end = std::chrono::high_resolution_clock::now();
    double time_run = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
    
    // std::cout << "   (-) Free-list after initialing:" << std::endl;
    // cpBuddy->Dump_FreeList();

    if (time_run < 1){
        time_run = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count();
        // std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(2) << time_run  << " nanoSec."<< std::endl;

    }
    else
        // std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(2) << time_run  << " microSec."<< std::endl;

    // std::cout << "\r\n\r\n";

    
    
    //--------------------------------Allocation---------------------------------------
    // std::cout << "(*) Allocation: --------------------------------" << std::endl;
    start = std::chrono::high_resolution_clock::now();
    std::ofstream filecsv;
    filecsv.open("./time_run.csv",std::ios_base::app);

    for (int k=0;k<loop;k++){
        start = std::chrono::high_resolution_clock::now();
        cpBuddy->Do_Allocate((0x30+k*NUM_REQ_PAGE+1),NUM_REQ_PAGE);
        end = std::chrono::high_resolution_clock::now();
        filecsv<<scenario<<","<<NUM_REQ_PAGE<<",";
        time_run = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
        if (time_run < 1){
        time_run = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count();
         filecsv<<time_run  << ",ns\n";
        }
        else
         filecsv<<time_run<<",ms\n";
        
    }
    
    filecsv.close();
    
    // cpBuddy->Dump_MappingTable();
    // cpBuddy->Dump_FreeList();
    // std::cout << "Average Block Size of 0x30: " << cpBuddy->Get_Current_Average_Block_Size() << std::endl;


    cpBuddy->Do_Allocate(0x50, 2000);
    // cpBuddy->Dump_MappingTable();
    // cpBuddy->Dump_FreeList();
    // std::cout << "Average Block Size of 0x50: " << cpBuddy->Get_Current_Average_Block_Size() << std::endl;

    cpBuddy->Do_Allocate(0x70, 30000);
    // cpBuddy->Dump_MappingTable();
    // cpBuddy->Dump_FreeList();
    // std::cout << "Average Block Size of 0x70: " << cpBuddy->Get_Current_Average_Block_Size() << std::endl;

    end = std::chrono::high_resolution_clock::now();
    time_run = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
    
    // std::cout << "   (-) Mapping table after allocating:" << std::endl;
    // cpBuddy->Dump_MappingTable();

    // std::cout << "   (-) Free-list after allocating:" << std::endl;
    // cpBuddy->Dump_FreeList();

    if (time_run < 1){
        time_run = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count();
        // std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(2) << time_run  << " nanoSec."<< std::endl;

    }
    else
        // std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(2) << time_run  << " microSec."<< std::endl;

    // std::cout << "\r\n\r\n";
    

    
    
    //--------------------------------De-allocation---------------------------------------
    // std::cout << "(*) De-allocation: --------------------------------" << std::endl;
    start = std::chrono::high_resolution_clock::now();

    cpBuddy->Do_Deallocate(0x70, 30000);
    // cpBuddy->Dump_MappingTable();
    // cpBuddy->Dump_FreeList();

    cpBuddy->Do_Deallocate(0x50, 2000);
    // cpBuddy->Dump_MappingTable();
    // cpBuddy->Dump_FreeList();

    cpBuddy->Do_Deallocate(0x30, 1000);
    // cpBuddy->Dump_MappingTable();
    // cpBuddy->Dump_FreeList();

    end = std::chrono::high_resolution_clock::now();
    time_run = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
    
    // std::cout << "   (-) Mapping table after de-allocating:" << std::endl;
    // cpBuddy->Dump_MappingTable();

    // std::cout << "   (-) Free-list after de-allocating:" << std::endl;
    // cpBuddy->Dump_FreeList();

    if (time_run < 1){
        time_run = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count();
         std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(2) << time_run  << " nanoSec."<< std::endl;
    }
    else
         std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(2) << time_run  << " microSec."<< std::endl;

    delete(cpBuddy);

    return 0;
}