#!/bin/bash
if [ ! -d "output" ]
then
    mkdir "output"
else
    rm -rf "output"
    mkdir "output"
fi
for i in $(seq 5); 
    do
        echo "* Run Test1-$i";
        ./BuddyTest testcase/testbench/Test1 > output/Test1-$i.txt
        echo "--> Done Test1-$i";
    done
echo "--> Done all test cases."