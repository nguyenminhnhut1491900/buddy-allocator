Test cases 1 and 2 use for testing with small quantity of requirement pages and requirement time.

All test case (from 3 to 8) have the same config file:
Memory space is 2GB
MAX_ORDER = 10

Test cases 3, 5, 7 have the same alloc file (require 10.000 pages)
Test cases 2, 4, 6 have the same alloc file (require 100 pages)

Test cases 3 and 4 use for case 1: Initially all memory is free (same freelist file)
Test cases 5 and 6 use for case 1: Initially all memory is fragmented (same freelist file)
Test cases 7 and 8 use for case 1: Initially all memory is (randomly) fragmented (same freelist file)