#include "BuddyAllocator.h"
#include <chrono>

int main(int argc, char* argv[]){
    
    char *alloc_base;

    /*
        100 pages requires;
        MAX_ORDER = 7 -> 128 pages;
        All memory is free;
        StartVPN = 0;
    */

    //--------------------------------Initial FreeList---------------------------------------
    std::cout << "(*) Initial FreeList: --------------------------------" << std::endl;
    auto start = std::chrono::high_resolution_clock::now();

    Buddy_Allocator * Example = new Buddy_Allocator(7);
    Example->set_startVPN(0);
    Example->addFL(6,0);
    Example->addFL(6,64);

    auto end = std::chrono::high_resolution_clock::now();
    double time_run = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
    
    std::cout << "   (-) Free-list after initialing:" << std::endl;
    Example->dump_freeList();

    std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(9) << time_run  << " uSec."<< std::endl;

    std::cout << "\r\n\r\n";

    //--------------------------------Allocation---------------------------------------
    std::cout << "(*) Allocation: --------------------------------" << std::endl;
    start = std::chrono::high_resolution_clock::now();

    //Allocate 100 pages in one time
    Example->buddy_allocate(100);

    /*
    //Allocate 100 times, one page per time
    for(int i=0; i< 100; i++)
        Example->buddy_allocate(1);
    */

    end = std::chrono::high_resolution_clock::now();
    time_run = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
    
    std::cout << "   (-) Free-list after allocating:" << std::endl;
    Example->dump_freeList();

    std::cout << "   (-) Mapping table after allocating:" << std::endl;
    Example->dump_mappingTable();

    std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(9) << time_run  << " uSec."<< std::endl;
       
    std::cout << "\r\n\r\n";
    

    //--------------------------------De-allocation---------------------------------------
    std::cout << "(*) De-allocation: --------------------------------" << std::endl;
    start = std::chrono::high_resolution_clock::now();

    Example->buddy_deallocate();

    end = std::chrono::high_resolution_clock::now();
    time_run = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
    
    std::cout << "   (-) Free-list after de-allocating:" << std::endl;
    Example->dump_freeList();

    std::cout << "   (-) Mapping table after de-allocating:" << std::endl;
    Example->dump_mappingTable();

    std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(9) << time_run  << " uSec."<< std::endl;
       
    


    return 0;
}