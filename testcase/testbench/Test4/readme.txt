* Case 1: ----------------------------
Application requires 100 pages
Memory size = 2GB = 2^31 = 2^10 (pages) * 2^21 (Bytes/Page)
=> Page size = 2^21 Bytes = 2097152
MAX_ORDER = 10

Initially all memory is free
Start Virtual Page Number = 0
