#include "CBuddy.h"
#include <iostream>
#include <fstream>
#include <chrono>

bool fexists(const std::string& filename){
    std::ifstream ifile(filename);
    return (bool) ifile;
}

int main(int argc, char* argv[]){
    
    if (argc < 2) return 0;

    std::string root(argv[1]);

    // std::string root = "testcase/testbench/Test1";

    std::string configPath = root + "/config.txt";
    std::string freelistPath = root + "/freelist.txt";
    std::string allocPath = root + "/alloc.txt";

    if (!fexists(configPath)) return 0;
    if (!fexists(freelistPath)) return 0;
    if (!fexists(allocPath)) return 0;


    //Open config file 
    int max_order, page_size, startVPN;
    std::fstream config(configPath, std::ios_base::in);
    config >> max_order >> page_size >> startVPN;
    
    //Open freelist file
    int freelist_A, freelist_B;
    std::fstream freelist(freelistPath, std::ios_base::in);


    //Open alloc file
    int allocNumPage;
    std::fstream alloc(allocPath, std::ios_base::in);
    std::fstream dealloc(allocPath, std::ios_base::in);



    //--------------------------------Initial FreeList---------------------------------------
    std::cout << "(*) Initial FreeList: --------------------------------" << std::endl;
    auto start = std::chrono::high_resolution_clock::now();

    CBuddy * cpBuddy = new CBuddy("Buddy_Allocator");

    while (freelist >> freelist_A >> freelist_B){
        cpBuddy->addToFreeList(freelist_A, freelist_B);
    }

    auto end = std::chrono::high_resolution_clock::now();
    double time_run = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
    
    std::cout << "   (-) Free-list after initialing:" << std::endl;
    cpBuddy->dump_freeList();

    if (time_run < 1){
        time_run = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count();
        std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(2) << time_run  << " nanoSec."<< std::endl;

    }
    else
        std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(2) << time_run  << " microSec."<< std::endl;

    std::cout << "\r\n\r\n";

    
    
    //--------------------------------Allocation---------------------------------------
    std::cout << "(*) Allocation: --------------------------------" << std::endl;
    start = std::chrono::high_resolution_clock::now();

    while (alloc >> allocNumPage){
        cpBuddy->Do_Allocate(0x30, allocNumPage);
    }

    end = std::chrono::high_resolution_clock::now();
    time_run = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
    
    std::cout << "   (-) Free-list after allocating:" << std::endl;
    cpBuddy->dump_freeList();

    std::cout << "   (-) Mapping table after allocating:" << std::endl;
    cpBuddy->dump_mappingTable();

    if (time_run < 1){
        time_run = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count();
        std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(2) << time_run  << " nanoSec."<< std::endl;

    }
    else
        std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(2) << time_run  << " microSec."<< std::endl;

    std::cout << "\r\n\r\n";
    

    
    
    //--------------------------------De-allocation---------------------------------------
    std::cout << "(*) De-allocation: --------------------------------" << std::endl;
    start = std::chrono::high_resolution_clock::now();

    cpBuddy->Do_Deallocate(0x30, 4);

    end = std::chrono::high_resolution_clock::now();
    time_run = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
    
    std::cout << "   (-) Free-list after de-allocating:" << std::endl;
    cpBuddy->dump_freeList();

    std::cout << "   (-) Mapping table after de-allocating:" << std::endl;
    cpBuddy->dump_mappingTable();

    if (time_run < 1){
        time_run = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count();
        std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(2) << time_run  << " nanoSec."<< std::endl;

    }
    else
        std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(2) << time_run  << " microSec."<< std::endl;

    return 0;
}